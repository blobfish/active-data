activeDataSources = [
  'ActData_Common.cpp'
  , 'ActData_Plugin.cpp']
  
driverSources = [
  'BinDrivers/ActData_BinDrivers.cpp'
  , 'BinDrivers/ActData_BinRetrievalDriver.cpp'
  , 'BinDrivers/ActData_BinStorageDriver.cpp'
  , 'BinDrivers/ActData_MeshDriver.cpp']

kernelSources = [ 
  'Kernel/ActData_Application.cpp'
  , 'Kernel/ActData_AsciiStringParameter.cpp'
  , 'Kernel/ActData_BaseModel.cpp'
  , 'Kernel/ActData_BaseNode.cpp'
  , 'Kernel/ActData_BasePartition.cpp'
  , 'Kernel/ActData_BaseTreeFunction.cpp'
  , 'Kernel/ActData_BoolArrayParameter.cpp'
  , 'Kernel/ActData_BoolParameter.cpp'
  , 'Kernel/ActData_ComplexArrayParameter.cpp'
  , 'Kernel/ActData_CopyPasteEngine.cpp'
  , 'Kernel/ActData_DependencyAnalyzer.cpp'
  , 'Kernel/ActData_DependencyGraph.cpp'
  , 'Kernel/ActData_DependencyGraphIterator.cpp'
  , 'Kernel/ActData_ExtTransactionEngine.cpp'
  , 'Kernel/ActData_FuncExecutionCtx.cpp'
  , 'Kernel/ActData_FuncExecutionTask.cpp'
  , 'Kernel/ActData_GroupParameter.cpp'
  , 'Kernel/ActData_IntArrayParameter.cpp'
  , 'Kernel/ActData_IntParameter.cpp'
  , 'Kernel/ActData_LogBook.cpp'
  , 'Kernel/ActData_LogBookAttr.cpp'
  , 'Kernel/ActData_MeshParameter.cpp'
  , 'Kernel/ActData_MetaParameter.cpp'
  , 'Kernel/ActData_NameParameter.cpp'
  , 'Kernel/ActData_NodeFactory.cpp'
  , 'Kernel/ActData_ParameterDTO.cpp'
  , 'Kernel/ActData_ParameterFactory.cpp'
  , 'Kernel/ActData_RealArrayParameter.cpp'
  , 'Kernel/ActData_RealParameter.cpp'
  , 'Kernel/ActData_RefClassifier.cpp'
  , 'Kernel/ActData_ReferenceListParameter.cpp'
  , 'Kernel/ActData_ReferenceParameter.cpp'
  , 'Kernel/ActData_SamplerTreeNode.cpp'
  , 'Kernel/ActData_SelectionParameter.cpp'
  , 'Kernel/ActData_SequentialFuncIterator.cpp'
  , 'Kernel/ActData_ShapeParameter.cpp'
  , 'Kernel/ActData_StringArrayParameter.cpp'
  , 'Kernel/ActData_TimeStampParameter.cpp'
  , 'Kernel/ActData_TransactionEngine.cpp'
  , 'Kernel/ActData_TreeFunctionParameter.cpp'
  , 'Kernel/ActData_TreeNodeParameter.cpp'
  , 'Kernel/ActData_TriangulationParameter.cpp'
  , 'Kernel/ActData_UserParameter.cpp'
  , 'Kernel/ActData_Utils.cpp']
 
meshSources = [ 
  'Mesh/ActData_MeshAttr.cpp'
  , 'Mesh/ActData_MeshDeltaEntities.cpp'
  , 'Mesh/ActData_MeshMDelta.cpp']
 
meshDataSources = [
  'Mesh/DS/ActData_Mesh.cpp'
  , 'Mesh/DS/ActData_Mesh_Direction.cpp'
  , 'Mesh/DS/ActData_Mesh_Edge.cpp'
  , 'Mesh/DS/ActData_Mesh_Element.cpp'
  , 'Mesh/DS/ActData_Mesh_ElementsIterator.cpp'
  , 'Mesh/DS/ActData_Mesh_Face.cpp'
  , 'Mesh/DS/ActData_Mesh_Group.cpp'
  , 'Mesh/DS/ActData_Mesh_IDFactory.cpp'
  , 'Mesh/DS/ActData_Mesh_MapOfMeshElement.cpp'
  , 'Mesh/DS/ActData_Mesh_MapOfMeshOrientedElement.cpp'
  , 'Mesh/DS/ActData_Mesh_Node.cpp'
  , 'Mesh/DS/ActData_Mesh_Position.cpp'
  , 'Mesh/DS/ActData_Mesh_Quadrangle.cpp'
  , 'Mesh/DS/ActData_Mesh_Triangle.cpp']

patternSources = [
  'Patterns/ActData_RealArrayOwnerAPI.cpp'
  , 'Patterns/ActData_RecordCollectionOwnerAPI.cpp']

standardSources = [
  'STD/ActData_BaseVarNode.cpp'
  , 'STD/ActData_BoolVarNode.cpp'
  , 'STD/ActData_BoolVarPartition.cpp'
  , 'STD/ActData_IntVarNode.cpp'
  , 'STD/ActData_IntVarPartition.cpp'
  , 'STD/ActData_RealEvaluatorFunc.cpp'
  , 'STD/ActData_RealVarNode.cpp'
  , 'STD/ActData_RealVarPartition.cpp']

toolsSources = [
  'Tools/ActData_CAFConversionAsset.cpp'
  , 'Tools/ActData_CAFConversionCtx.cpp'
  , 'Tools/ActData_CAFConversionModel.cpp'
  , 'Tools/ActData_CAFConversionNode.cpp'
  , 'Tools/ActData_CAFConversionParameter.cpp'
  , 'Tools/ActData_CAFConverter.cpp'
  , 'Tools/ActData_CAFConverterFw.cpp'
  , 'Tools/ActData_CAFDumper.cpp'
  , 'Tools/ActData_CAFLoader.cpp'
  , 'Tools/ActData_GraphToDot.cpp'
  , 'Tools/ActData_UniqueNodeName.cpp']

adIncludeDirs = ['.', 'BinDrivers', 'Kernel', 'Mesh', 'Mesh/DS', 'Patterns', 'STD', 'Tools', occt.get_variable(cmake : 'OpenCASCADE_INCLUDE_DIR')]

activeDataLib = shared_library('ActiveData'
  , [activeDataSources, driverSources, kernelSources, meshSources, meshDataSources, patternSources, standardSources, toolsSources]
  , version : meson.project_version()
  , dependencies : [activeDataAPILibDep, activeDataAuxLibDep]
  , include_directories : include_directories(adIncludeDirs, is_system : true)
  , install : true)

activeDataLibDep = declare_dependency(include_directories : include_directories(adIncludeDirs, is_system : true)
  , link_with : [activeDataLib]
  , dependencies : [activeDataAPILibDep, activeDataAuxLibDep])
